# astra-cartago-counter

Sample CArtAgO Project taken from lesson 1 of the CArtAgO user guide. 

The lesson can be found at: http://cartago.sourceforge.net/?page_id=69

It contains the implementation of a basic counter and and example of its use.

# Running the Example

Please download and type:

'mvn compile astra:deploy'