package artifact;

import cartago.Artifact;
import cartago.OPERATION;

public class Counter extends Artifact {
  @OPERATION void init() {
    defineObsProperty("count", 0);
  }

  @OPERATION void inc() {
    int count = getObsProperty("count").intValue();
    updateObsProperty("count", count + 1);
  }
}
